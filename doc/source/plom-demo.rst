.. Plom documentation
   Copyright 2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

``plom-demo``
-------------

.. argparse::
   :ref: plom.scripts.demo.get_parser
   :prog: plom-demo
