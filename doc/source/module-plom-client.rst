.. Plom documentation
   Copyright 2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

``plom.client`` module
----------------------

Plom Client
^^^^^^^^^^^

.. automodule:: plom.client
    :members:
.. automodule:: plom.client.annotator
    :members:
